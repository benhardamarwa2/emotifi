# Emotifi

**`emotify`** is an innovative sentiment analysis project focused on analyzing song lyrics extracted from audio files. By leveraging cutting-edge deep learning technologies, **Emotifi** aims to classify the emotional tone of song lyrics. This sentiment analysis model enables the categorization of songs based on mood, facilitating the creation of mood-based playlists.

## Navigation

- [Emotifi](#emotifi)
  - [Navigation](#navigation)
  - [Introduction](#introduction)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Features](#features)
  - [Contributing](#contributing)
  - [License](#license)

## Introduction

**`emotify`** is a project born out of the desire to enhance music listenning experiences by providing a deeper understanding of the emotional content embedded within song lyrics. By employing state-of-the-art deep learning algorithms, **Emotifi** transforms audio data into text (lyrics) and analyzes the sentiment of the lyrics to classify songs into different emotional categories.

## Installation

To use **Emotifi**, follow these installation steps:

1. Clone the **`emotifi`** repository:

```bash
git clone git@gitlab.com:cosmobyte/emotifi.git
```

2. Navigate to the **emotify** folder:

```bash
cd emotifi
```

3. Create a virual environment and activate it:

```bash
python3 -m venv <name-of-vitual-env>
source <name-of-vitual-env>/bin/activate 
```

4. Install the required dependencies using pip:

```bash
pip install -r requirements.txt
```

## Usage

To analyze song lyrics using **`emotifi`**, follow these steps:

1. Prepare your audio file (s) containing the song (s) you want to analyze.

2. Run the **`emotifi`** script, specifying the path to the audio file:

```python
python analyze.py --audio path/to/audio/file
```

**Emotifi** will process the audio file (s), extract the lyrics, and perform sentiment analysis to categorize the song's mood.

## Features

* **Sentiment Analysis**: Emotifi employs deep learning models to analyze the sentiment of song lyrics extracted from audio files.

* **Mood Classification**: Songs are categorized into different emotional tones, allowing users to create mood-based playlists effotlessly.
  

## Contributing

Contributions to **`emotifi`** are welcome! If you have ideas for improvements, new features, or bug fixes, feel free to open an issue or submit a pull request on the GitLab repository.

## License


