# Makefile for Emotifi

## Commmand to download data
data:
    python src/data/download_data.py

## Command to preprocess data
preprocess:
    python src/data/preprocess_data.py

## Command to train models
train:
    python src/models/train_model.py

## Command to generate reports
generate_report:
    python src/reports/generate_report.py
